#! /usr/bin/python

import sys, math
from collections import defaultdict

class Em:
	def __init__(self):
		self.counts_ef = defaultdict(float)
		self.counts_e = defaultdict(float)
		self.counts_ilmj = defaultdict(float)
		self.counts_ilm = defaultdict(float)
		self.trans = defaultdict(float)
		self.de_set = set()
		self.q = defaultdict(float)

# trans_counts just computes the number of unique words
# in the german corpus

	def trans_counts(self):
		de_corpus = open('corpus.de','r')
		de = de_corpus.readline().strip().split()
		while de:
			for j in xrange(0, len(de)):
				self.de_set.add(de[j])
			de = de_corpus.readline().strip().split()
		
		return len(self.de_set)

#init_trans initializes the t parameters

	def init_trans(self, denom):
		de_corpus = open('corpus.de','r')
		en_corpus = open('corpus.en', 'r')
		de = de_corpus.readline().strip().split()
		en = en_corpus.readline().strip().split()
		while de and en:
			for m in xrange(0, len(de)):
				for l in xrange(0, len(en)):
					self.trans[(en[l], de[m])] = float(1.0/denom)
				self.trans[("NULL", de[m])] = float(1.0/denom)
			de = de_corpus.readline().strip().split()
			en = en_corpus.readline().strip().split()

# sum_trans calcululates the denominator for the 
# delta(k,i,j) constant for IBM 1

	def sum_trans(self,de,en,s,pr):
		total = 0.0
		for i in xrange (0,len(en)):
			if (s == 0):
				total = total + pr
			else:				
				total = total + self.trans[(en[i], de)]
		return total

# sum_trans calcululates the denominator for the 
# delta(k,i,j) constant for IBM 2		

	def sum_qt(self,i,de_len,de_word,en,s):
		total = 0.0
		for j in xrange (0,len(en)):
			if (s == 0):
				q = 1.0/(len(en)+1)
			else:
				q = self.q[(i,len(en),de_len,j)]
			total = total + q*self.trans[(en[j], de_word)]
		return total

# E-M for IBM 1

	def em(self, max_iter):
		tc = float(1.0/self.trans_counts())
		de_corpus = open('corpus.de','r')
		en_corpus = open('corpus.en','r')
		for s in xrange(0,max_iter):
			self.counts_ef = defaultdict(float)
			self.counts_e = defaultdict(float)
			for k in xrange(20000): #Enough to get to the end of the corpora
				de_split = de_corpus.readline().strip().split()
				en_split = en_corpus.readline().strip().split()
				en_split.insert(0, "NULL")
				for i in xrange(0, len(de_split)):
					denom_delta = self.sum_trans(de_split[i],en_split,s,tc)
					for j in xrange(0, len(en_split)):
						if (s == 0):
							t = float(1.0/tc)
						else:
							t = self.trans[(en_split[j], de_split[i])]
						delta = t / denom_delta
						self.counts_ef[(en_split[j], de_split[i])] += delta
						self.counts_e[(en_split[j])] += delta
			for (e,f) in self.counts_ef: #updating t params
				self.trans[(e, f)] = self.counts_ef[(e,f)]/self.counts_e[(e)]	
			de_corpus.seek(0)
			en_corpus.seek(0)

# E-M for IBM 2

	def em2(self, max_iter):
		de_corpus = open('corpus.de','r')
		en_corpus = open('corpus.en','r')
		for s in xrange(0,max_iter):
			self.counts_ef = defaultdict(float)
			self.counts_e = defaultdict(float)
			for k in xrange(20000): #Enough to get to the end of the corpora
				de_split = de_corpus.readline().strip().split()
				en_split = en_corpus.readline().strip().split()
				en_split.insert(0, "NULL")
				el = float(1.0/(len(en_split)+1))
				for i in xrange(0, len(de_split)):
					denom_delta = self.sum_qt(i,len(de_split),de_split[i],en_split,s)
					for j in xrange(0, len(en_split)):
						if (s == 0):
							q = el
						else:
							q = self.q[(i,len(en_split),len(de_split),j)]
						t = float(self.trans[(en_split[j], de_split[i])])
						delta = float((q*t) / denom_delta )
						self.counts_ef[(en_split[j], de_split[i])] += delta
						self.counts_e[(en_split[j])] += delta
						self.counts_ilmj[(i,len(en_split),len(de_split),j)] += delta
						self.counts_ilm[(i,len(en_split),len(de_split))] += delta
			for (e,f) in self.counts_ef: # updating t params
				self.trans[(e, f)] = self.counts_ef[(e,f)]/self.counts_e[(e)]
			for (i,l,m,j) in self.counts_ilmj: # updating q params
				self.q[(i,l,m,j)] = self.counts_ilmj[(i,l,m,j)]/self.counts_ilm[(i,l,m)]
			de_corpus.seek(0)
			en_corpus.seek(0)

# gets ten more probable foreign words for english
# words in devwords.txt

	def get_ten(self):
		ten_file = open('ten', 'w')
		dev_words = open('devwords.txt', 'r')
		line = dev_words.readline()
		while line:
			line = line.strip()
			a = []
			for (e, f) in self.trans:
				if (e == line):
					a.append((self.trans[(e,f)], f))
			a.sort(reverse=True) # orders descending probabilities
			highest = a[:10]
			for i in xrange(0,len(highest)): #needed tuples with prob first to sort
				(a,b) = highest[i]
				highest[i] = (b,a) # reverses tuples for print out
			ten_file.write(line + "\n" + str(highest) + "\n\n")
			line = dev_words.readline()

# align does Question 4 alignments

	def align(self):
		align_file = open('alignments', 'w')
		de_corpus = open('corpus.de','r')
		en_corpus = open('corpus.en','r')
		for k in xrange(20):
			alignments = []
			de = de_corpus.readline()
			de_split = de.strip().split()
			en = en_corpus.readline()
			en_split = en.strip().split()
			en_split.insert(0, "NULL")
			for i in xrange(0, len(de_split)):
				high_val = 0.0
				high_ind = -1
				for j in xrange(0, len(en_split)):
					val = self.trans[(en_split[j], de_split[i])]
					if val > high_val:
						high_ind = j
						high_val = val
				alignments.append(high_ind)
			align_file.write(en + de + str(alignments) + "\n\n")

# align2 does question 5 alignments

	def align2(self):
		align_file = open('alignments2', 'w')
		de_corpus = open('corpus.de','r')
		en_corpus = open('corpus.en','r')
		for k in xrange(20):
			alignments = []
			de = de_corpus.readline()
			de_split = de.strip().split()
			en = en_corpus.readline()
			en_split = en.strip().split()
			en_split.insert(0, "NULL")
			for i in xrange(0, len(de_split)):
				high_val = 0.0
				high_ind = -1
				for j in xrange(0, len(en_split)):
					val = float(self.trans[(en_split[j], de_split[i])]*self.q[i,len(en_split),len(de_split),j])
					if (val >= high_val):
						high_ind = j
						high_val = val
				alignments.append(high_ind)
			align_file.write(en + de + str(alignments) + "\n\n")

# unscrambles for question 6. Iterates over all german sentences,
# then all english sentences, then over the german words of the
# current sentence, then the words of the english sentence.
# The english sentence with the highest probability is printed

	def unscramble(self):
		unscramble_file = open('unscram_results','w')
		de_corpus = open('original.de','r')
		en_corpus = open('scrambled.en','r')
		for g in de_corpus:
			de_split = g.strip().split()
			top_pr = float('-inf')
			top_e = "error\n"
			for e in en_corpus:
				en_split = e.strip().split()
				en_split.insert(0, "NULL")
				ttl = 0.0
				for i in xrange(0, len(de_split)):
					high_val = 0.0
					for j in xrange(0, len(en_split)):
						val = float(self.trans[(en_split[j], de_split[i])]*self.q[i,len(en_split),len(de_split),j])
						if (val >= high_val):
							high_val = val
					if (high_val == 0):
						ttl -= 1000
					else:
						ttl += math.log(high_val)
				if (ttl >= top_pr):
					top_pr = ttl
					top_e = e
			unscramble_file.write(top_e)
			en_corpus.seek(0)

# This performs exactly the same as unscramble but
# is structured a little more like the math in the notes

	def unscramble2(self):
		unscramble_file = open('unscram_results','w')
		de_corpus = open('original.de','r')
		en_corpus = open('scrambled.en','r')
		for g in de_corpus:
			de_split = g.strip().split()
			top_pr = float('-inf')
			top_e = "error\n"
			for e in en_corpus:
				alignments = []
				en_split = e.strip().split()
				en_split.insert(0, "NULL")
				ttl = 0.0
				for i in xrange(0,len(de_split)):
					high_val = 0.0
					high_ind = -1
					for j in xrange(0, len(en_split)):
						val = float(self.trans[(en_split[j], de_split[i])]*self.q[i,len(en_split),len(de_split),j])
						if (val >= high_val):
							high_val = val
							high_ind = j
					alignments.append(high_ind)
				for k in xrange(0,len(de_split)):
					prod = float(self.q[(k,len(en_split),len(de_split),alignments[k])]*self.trans[(en_split[alignments[k]]),de_split[k]])
					if (prod == 0.0):
						ttl -= 10000
					else:
						ttl += math.log(prod)
				if (ttl > top_pr):
					top_pr = ttl
					top_e = e
			unscramble_file.write(top_e)
			en_corpus.seek(0)

if __name__ == "__main__": 
	cts = Em()
	cts.em(5)
	cts.get_ten()
	cts.align()
	cts.em2(5)
	cts.align2()
	cts.unscramble()